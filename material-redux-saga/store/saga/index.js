import {
    all,
    takeEvery,
    takeLatest,
    call,
    select,
    put
} from "redux-saga/effects";
function* handleUpdate(action){
    console.log(action);
}
export default function* rootSaga() {
    yield all([
        yield takeLatest('jsonforms/UPDATE',handleUpdate),
    ]);
}