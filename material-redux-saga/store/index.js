import { createStore, applyMiddleware, compose} from "redux";
import createSagaMiddleware from 'redux-saga';
import { persistStore, persistCombineReducers} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers';
import rootSaga from "./saga";
import { materialCells, materialRenderers} from "@jsonforms/material-renderers";
const  configureStore= (function () {
    var store;

    function createMyStore() {
        /**
         * Recreate the stdChannel (saga middleware) with every context.
         */
        const sagaMiddleware = createSagaMiddleware()
        const composeEnhancers = typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
        const logger = createLogger({
            collapsed: (state, action) => {
                if (!action.type) {
                    return false;
                }
                return action.type.includes('jsonforms')
            },
            // diff: true,
        });
        /**
         * Since Next.js does server-side rendering, you are REQUIRED to pass
         * `preloadedState` when creating the store.
         */
           const  tstore = createStore(
            rootReducer,
            {
                jsonforms: {
                    cells: materialCells,
                    renderers: materialRenderers
                }
            },
            composeEnhancers(applyMiddleware(sagaMiddleware)),
        )
        /**
         * next-redux-saga depends on `sagaTask` being attached to the store.
         * It is used to await the rootSaga task before sending results to the client.
         */
        //const persistor = persistStore(store);
        tstore.sagaTask = sagaMiddleware.run(rootSaga)
        return tstore;
    }

    return {
        getStore: function () {
            if (!store) {
                store = createMyStore();
            }
            return store;
        }
    };
})();

export default configureStore.getStore;